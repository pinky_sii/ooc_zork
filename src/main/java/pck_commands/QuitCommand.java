package pck_commands;

import pck_game.Player;

/**
 * Created by Siripatsornsirichai on 1/28/2018 AD.
 */
public class QuitCommand extends Command {
    public boolean execute(Player player) {
        System.out.println("Thank you for playing");
        return true;
    }
}
