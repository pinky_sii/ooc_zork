package pck_commands;

import pck_game.Player;

/**
 * Created by Siripatsornsirichai on 1/28/2018 AD.
 */
public class InfoCommand extends Command {
    public boolean execute(Player player) {

        System.out.println("- - - - - - - - - - - - - - - - - - - - - - - - - - - - -");
        System.out.println("[player] ");
        System.out.println("+ name: "+player.getName());
        System.out.println("+ HP: " +player.getHP());
        System.out.print("+ inventory ");
        player.getALLItem();

        System.out.println("- - - - - - - - - - - - - - - - - - - - - - - - - - - - -");

        System.out.println("[room] ");
        System.out.println("+ current room: " + player.getCurrentRoom().getDescription());
        System.out.print("+ items: ");
        player.getCurrentRoom().getALLItem();
        System.out.print("+ monster: ");
        player.getCurrentRoom().getAllCharacters();

        System.out.println("- - - - - - - - - - - - - - - - - - - - - - - - - - - - -");
        System.out.println("[map]");
        System.out.println("number of monsters: "+ player.getMyMap().getNumOfMonster());

        return false;
    }
}
