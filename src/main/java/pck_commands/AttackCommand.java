package pck_commands;

import pck_characters.Character;
import pck_game.Player;
import pck_items.Item;
import pck_room.MapOfGame;
import pck_room.Room;

import java.util.Random;

/**
 * Created by Siripatsornsirichai on 1/29/2018 AD.
 */
public class AttackCommand extends Command {
    public boolean execute(Player player) {


        if (!this.hasArgument()) {
            System.out.println("Attack what..?");
            return false;
        }

        String monsterName = this.getArgument();
        Character monster = player.getCurrentRoom().findCharacter(monsterName);

        if (monster == null) {
            System.out.println("no such monster in this room");
        } else {
            // attack
            Random ran = new Random();

            int playRan = ran.nextInt(10);
            int monsRan = ran.nextInt(10);

            int playerAttackPow = player.getAttackPower();
            int monAttackPow = monster.getAttackPower();

            if (playRan >= monsRan) {
                monster.setHP(playerAttackPow);

                System.out.println("yassss");
                System.out.println();
                System.out.println("Player HP: " + player.getHP());
                System.out.println(monster.getName() + " HP:" + monster.getHP());

                if (monster.getHP() <= 0) {
                    player.getMyMap().removeCharacter(player.getPosX(),player.getPosY(),monster);
                    System.out.println(monster.getName()+" is killed");
                }


            } else {
                player.setHP(monAttackPow);
                System.out.println("player got attacked");
                System.out.println();
                System.out.println("Player HP: " + player.getHP());
                System.out.println(monster.getName() + " HP:" + monster.getHP());

                if (player.getHP() <= 0) {
                    System.out.println("you got attacked by " + monster.getName());
                    System.out.println("Game over. Bye bye !!");
                    System.exit(0);
                }
            }
        }
        return false;
    }
}
