package pck_commands;

import pck_game.Player;
import pck_room.MapOfGame;

/**
 * Created by Siripatsornsirichai on 1/29/2018 AD.
 */
public class WestCommand extends Command {
    public boolean execute(Player player) {

        int cuuX = player.getPosX();
        int cuuY = player.getPosY();

        MapOfGame level = player.getMyMap();

        if (cuuX < 0 || cuuX > 4 || cuuY-1 < 0 || cuuY-1 > 4) {
            System.out.println("There is no door !!");
        } else if (level.getRoom(cuuX,cuuY-1) != null) {
            player.setPosition(cuuX,cuuY-1);
            System.out.println("you are in " + player.getCurrentRoom().getDescription());
        }
        return false;
    }
}
