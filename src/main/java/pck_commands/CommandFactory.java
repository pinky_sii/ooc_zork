package pck_commands;


import java.util.HashMap;
import java.util.Map;

/**
 * Created by Siripatsornsirichai on 1/28/2018 AD.
 */
public class CommandFactory {

    private Map<String, Command> validCommands = new HashMap<String, Command>() {
        {

            put("north", new NorthCommand());
            put("south", new SouthCommand());
            put("west", new WestCommand());
            put("east", new EastCommand());

            put("info", new InfoCommand());
            put("take", new TakeCommand());
            put("drop", new DropCommand());

            put("quit", new QuitCommand());
            put("help", new HelpCommand());

            put("eat", new EatCommand());
            put("attack", new AttackCommand());

        }
    };

    public Command getValidCommand(String command) {
        return validCommands.get(command);
    }

//    public void getAllValidCommands(){
//        for (String cmd: validCommands.keySet()) {
//            System.out.print(cmd+ ", ");
//        }
//        System.out.println();
//    }
//
//    public boolean isCommand(String command) {
//        return validCommands.containsKey(command);
//    }

}
