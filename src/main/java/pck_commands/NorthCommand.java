package pck_commands;

import pck_game.Player;
import pck_room.MapOfGame;

/**
 * Created by Siripatsornsirichai on 1/29/2018 AD.
 */
public class NorthCommand extends Command {
    public boolean execute(Player player) {

        int cuuX = player.getPosX();
        int cuuY = player.getPosY();

        MapOfGame level = player.getMyMap();

        if (cuuX-1 < 0 || cuuX-1 > 4 || cuuY < 0 || cuuY > 4) {
            System.out.println("There is no door !!");
        } else if (level.getRoom(cuuX-1,cuuY) != null) {
            player.setPosition(cuuX-1,cuuY);
            System.out.println("you are in " + player.getCurrentRoom().getDescription());
        }
        return false;
    }
}
