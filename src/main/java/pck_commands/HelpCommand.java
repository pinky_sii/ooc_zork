package pck_commands;

import pck_game.Player;

/**
 * Created by Siripatsornsirichai on 1/28/2018 AD.
 */
public class HelpCommand extends Command  {
    public boolean execute(Player player) {
        CommandFactory commands = new CommandFactory();
        System.out.println("Your command words are: ");
        System.out.println("north - go north");
        System.out.println("south - go south");
        System.out.println("east  - go east");
        System.out.println("west  - go west");
        System.out.println("take  - to take an item to your inventory");
        System.out.println("drop  - to drop an item from your inventory to the room");
        System.out.println("eat   - to eat an item from your inventory");
        System.out.println("info  - information of player and current room");
        System.out.println("quit  - to quit the game");

        return false;
    }
}
