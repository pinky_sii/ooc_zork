package pck_commands;

import java.util.Scanner;

/**
 * Created by Siripatsornsirichai on 1/28/2018 AD.
 */
public class Parser {
    private CommandFactory validCommands = new CommandFactory();

    public Command getCommand(){

        String commandName = null;
        String argument = null;

        System.out.print("> ");

        String inputCommand = new Scanner(System.in).nextLine();

        Scanner commandWord = new Scanner(inputCommand);
        if(commandWord.hasNext()) {
            commandName = commandWord.next();

            if (commandWord.hasNext()) {
                argument = commandWord.next();
            }
        }

        Command command = validCommands.getValidCommand(commandName);
        if (command != null) {
            command.setArgument(argument);
        }
        return command;

    }
}
