package pck_commands;

import pck_game.Player;
import pck_items.Item;

/**
 * Created by Siripatsornsirichai on 1/28/2018 AD.
 */
public class DropCommand extends Command {
    public boolean execute(Player player) {

        if (!this.hasArgument()) {
            System.out.println("drop what..?");
            return false;
        }

        String itemName = this.getArgument();
        Item item = player.findItem(itemName);

        if (item == null) {
            System.out.println("no such item in your inventory");
        } else {
            player.removeItem(item);
            player.getCurrentRoom().addItem(item);
            System.out.println("drop " + item.getName());
        }
        return false;
    }
}
