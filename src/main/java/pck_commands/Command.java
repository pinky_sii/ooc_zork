package pck_commands;

import pck_game.Player;

/**
 * Created by Siripatsornsirichai on 1/28/2018 AD.
 */
public abstract class Command {
    private String argument;

    public Command() {
        this.argument = null;
    }

    public String getArgument() {
        return argument;
    }

    public void setArgument(String argument) {
        this.argument = argument;
    }

    public boolean hasArgument() {
        return argument!= null;
    }

    public abstract boolean execute(String[] args, Player player);
}
