package pck_commands;

import pck_game.Player;
import pck_items.Item;

/**
 * Created by Siripatsornsirichai on 1/28/2018 AD.
 */
public class TakeCommand extends Command {
    public boolean execute(Player player) {

        if (!this.hasArgument()) {
            System.out.println("take what..?");
            return false;
        }

        String itemName = this.getArgument();
        Item item = player.getCurrentRoom().findItem(itemName);

        if (item == null) {
            System.out.println("no such item in the room");
        } else {
            player.getCurrentRoom().removeItem(item);
            player.addItem(item);
            System.out.println("take 1 " + item.getName() + " to your inventory");
        }

        return false;
    }
}
