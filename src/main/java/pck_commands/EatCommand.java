package pck_commands;

import pck_game.Player;
import pck_items.Item;

/**
 * Created by Siripatsornsirichai on 1/29/2018 AD.
 */
public class EatCommand extends Command {
    public boolean execute(Player player) {

        if (!this.hasArgument()) {
            System.out.println("eat what..?");
            return false;
        }

        String itemName = this.getArgument();
        Item item = player.findItem(itemName);


        if (item == null) {
            System.out.println("no such item in your inventory");
        } else {
            if (item.isEdible()) {
                player.removeItem(item);    // remove edible item from inventory then eat
                player.eat(item);
                System.out.println("eat "+ item.getName()+ " +"+item.getHP()+" HP");
            }
        }
        return false;
    }
}
