package pck_game;

import pck_items.Inventory;
import pck_items.Item;
import pck_room.MapOfGame;
import pck_room.Room;

/**
 * Created by Siripatsornsirichai on 1/22/2018 AD.
 */
public class Player {
    private String name;
    private int HP;
    private Inventory inventory;
    private MapOfGame myMap;
    private Room currentRoom;
    private int PosX;
    private int PosY;
    private int attackPower;
    private int currentLevel;

//    private int defensePower;
//    private Room previousRoom;


    public Player(String name) {
        this.name = name;
        inventory = new Inventory();
        HP = 100;
        attackPower = 100;
    }

    public String getName(){
        return this.name;
    }

    // room
    public Room getCurrentRoom(){
        return this.currentRoom;
    }


    // inventory
    public void addItem(Item item) {
        inventory.addItem(item);
    }

    public void removeItem(Item item) {
        inventory.removeItem(item);
    }

    public void getALLItem() {
        inventory.getAllItems();
    }

    public Item findItem(String name) {
        return inventory.findItem(name);
    }


    // eat, attack
    public int getHP() {return this.HP;}

    public void setHP(int m) {this.HP-=m;}

    public int getAttackPower() {return this.attackPower;}

    public void eat(Item item) {this.HP += item.getHP();}


    // map
    public void setMyMap(MapOfGame lvl){
        myMap = lvl;
    }

    public void setPosition(int x, int y) {
        this.PosX = x;
        this.PosY = y;
        this.currentRoom = myMap.getRoom(x,y);
    }

    public MapOfGame getMyMap(){ return myMap;}

    public int getPosX(){
        return this.PosX;
    }

    public int getPosY(){
        return this.PosY;
    }


    // ... ?
    //public Room getPreviousRoom(){
//        return this.previousRoom;
//    }

//    public void changeRoom(Room currentRoom, Room nextRoom){
//        this.previousRoom = currentRoom;
//        this.currentRoom = nextRoom;
//    }


}




