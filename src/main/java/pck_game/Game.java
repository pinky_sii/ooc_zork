package pck_game;

/**
 * Created by Siripatsornsirichai on 1/22/2018 AD.
 *
 * New game
 *
 */

public class Game {

    private GameInit game;

    public Game(){
        game = new GameInit();
        game.play();
    }

    public static void main(String[] args) {
        new Game();
    }
}
