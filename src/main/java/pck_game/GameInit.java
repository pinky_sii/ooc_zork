package pck_game;

import pck_commands.Command;
import pck_commands.Parser;
import pck_room.Level;
import pck_room.MapOfGame;
import pck_room.Room;

/**
 * Created by Siripatsornsirichai on 1/24/2018 AD.
 *
 * Initailize all features of the game
 *
 */
public class GameInit {

    private Parser parser;
    private Player player;
    private Level levels;


    public GameInit(){

        parser = new Parser();
        player = new Player("Me");
        levels = new Level();

        MapOfGame level1 = levels.getLevel(0);

        MapOfGame level2 = levels.getLevel(1);

        MapOfGame level3 = levels.getLevel(2);

        player.setMyMap(levels.getLevel(0));

        player.setPosition(0,0);

    }

    public void checkObjective(MapOfGame currLevel, MapOfGame nextLevel) {
        if (currLevel.getNumOfMonster() == 0) {
            player.setMyMap(nextLevel);
            player.setPosition(0,0);
        }
    }

    private void printWelcome() {
        System.out.println("Welcome to the Mysterious House");
        System.out.println("Type 'help' if you need help");
        System.out.println("you are in " + player.getCurrentRoom().getDescription());

    }

    private boolean interpretCommand(Command command) {
        if (command == null) {
            System.out.println("i don't know what you mean by that..");
            return false;
        }
        return command.execute(player);
    }

    public void play() {
        printWelcome();

        boolean finished = false;
        while (!finished) {

            Command command = parser.getCommand();
            finished = interpretCommand(command);

            for (int i = 0; i < 2; i++) {
                if (player.getMyMap().getNumOfMonster() == 0) {

                    System.out.println();
                    System.out.println("- - - - Level complete!! - - - -");

                    int nextLev = i+2;
                    System.out.println("Welcome to level " + nextLev );
                    player.setMyMap(levels.getLevel(i+1));
                    player.setPosition(0, 0);
                    System.out.println("Now you are in  " + player.getCurrentRoom().getDescription());
                }
            }

        }
    }


}
