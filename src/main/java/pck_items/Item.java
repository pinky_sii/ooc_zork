package pck_items;

/**
 * Created by Siripatsornsirichai on 1/28/2018 AD.
 */
public class Item {
    private String name;
    private int HP;
    private boolean edible;

    public Item(String description, int HP, boolean edible) {
        name = description;
        this.HP = HP;
        this.edible = edible;
    }

    public String getName(){
        return this.name;
    }

    public int getHP(){
        return  this.HP;
    }

    public boolean isEdible(){
        return this.edible;
    }


}
