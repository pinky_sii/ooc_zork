package pck_items;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Siripatsornsirichai on 1/28/2018 AD.
 */
public class Inventory {
    private List<Item> inventory = new ArrayList<>();

    public void addItem(Item item){
        inventory.add(item);
    }

    public void removeItem(Item item) {
        inventory.remove(item);
    }

    public void getAllItems() {
        for (Item item: inventory) {
            System.out.print(item.getName()+", ");
        }
        System.out.println();
    }

    public Item findItem(String item) {
        for (Item it: inventory) {
            if (it.getName().equals(item)) {
                return it;
            }
        }
        return null;
    }


}
