package pck_room;

import pck_characters.Character;

/**
 * Created by Siripatsornsirichai on 1/29/2018 AD.
 */
public class MapOfGame {

    private Room[][] roomsMap = new Room[4][4];
    private Room startRoom;
    private int numOfMonster;
    //private boolean clear = false;


    public void addRoom(String name, int n, int m) {
        roomsMap[n][m] = new Room(name);
    }

    public Room getRoom(int x, int y){
        return roomsMap[x][y];
    }

    public void setStartRoom(int x, int y) {
        startRoom = roomsMap[x][y];
    }

    public void setItem(int n, int m, String item, int hp, boolean edible) {
        roomsMap[n][m].setItem(item,hp,edible);
    }

    public void setCharacter(int n, int m, String name, int HP) {
        roomsMap[n][m].setCharacter(name,HP);
        numOfMonster ++;
    }

    public void removeCharacter(int n, int m, Character name) {
        roomsMap[n][m].removeCharacter(name);
        numOfMonster--;
    }

    public int getNumOfMonster() {
        return numOfMonster;
    }



}
