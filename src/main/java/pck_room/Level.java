package pck_room;

/**
 * Created by Siripatsornsirichai on 1/29/2018 AD.
 */
public class Level {

    private MapOfGame[] levels;

    public Level() {
        levels = new MapOfGame[3];

        /* - - - - - - - Level 1 - - - - - - - - - -*/
        MapOfGame lvl1 = new MapOfGame();
        lvl1.addRoom("x1",0,0);
        lvl1.addRoom("x2",0,2);
        lvl1.addRoom("x3",1,0);
        lvl1.addRoom("x4",1,1);
        lvl1.addRoom("x5",1,2);
        lvl1.addRoom("x6",2,1);
        lvl1.addRoom("x7",2,2);

        lvl1.setItem(0,0,"cookie",20,true);
        lvl1.setItem(1,1,"cookie",20,true);

        lvl1.setCharacter(0,0,"baba", 50);
        //lvl1.setCharacter(1,1,"doge", 30);

        lvl1.setStartRoom(0,0);

        levels[0] = lvl1;

        /* - - - - - - - Level 2 - - - - - - - - - - - */
        MapOfGame lvl2 = new MapOfGame();
        lvl2.addRoom("b1",0,0);
        lvl2.addRoom("b2",0,1);
        lvl2.addRoom("b3",1,1);
        lvl2.addRoom("b4",2,0);
        lvl2.addRoom("b5",2,1);
        lvl2.addRoom("b6",2,2);
        lvl2.addRoom("b7",3,2);

        lvl2.setItem(0,0,"cookie",20,true);
        lvl2.setItem(1,1,"cookie",20,true);

        lvl2.setCharacter(0,0,"baba", 50);
        lvl2.setCharacter(1,1,"doge", 30);

        lvl2.setStartRoom(0,0);

        levels[1] = lvl2;


         /* - - - - - - - Level 3 - - - - - - - - - - - */
        MapOfGame lvl3 = new MapOfGame();

        lvl3.addRoom("c1",0,0);
        lvl3.addRoom("c2",1,0);
        lvl3.addRoom("c3",1,1);
        lvl3.addRoom("c4",1,2);
        lvl3.addRoom("c5",2,1);
        lvl3.addRoom("c5",2,2);
        lvl3.addRoom("c6",2,2);
        lvl3.addRoom("c7",3,1);
        lvl3.addRoom("c8",3,3);

        lvl1.setStartRoom(0,0);

        levels[2] = lvl3;

    }

    public MapOfGame getLevel(int n) {
        return levels[n];
    }

}
