package pck_room; /**
 * Created by Siripatsornsirichai on 1/22/2018 AD.
 */

import pck_characters.Character;
import pck_characters.NpcList;
import pck_items.Inventory;
import pck_items.Item;

import java.util.HashMap;
import java.util.Map;


/**
 *  Class pck_room.Room - a room in game
 *  contains exits, items, monsters
 *
 */

public class Room {

    private String description;
    private Inventory inventory;
    private NpcList characters;



    public Room(String description) {
        this.description = description;
        inventory = new Inventory();
        characters = new NpcList();
    }


    // set items and character to map
    public void setItem(String name, int hp, boolean edible) {
        Item item = new Item(name,hp,edible);
        inventory.addItem(item);
    }

    public void setCharacter(String name, int HP) {
        Character cha = new Character(name,HP);
        characters.addCharacter(cha);
    }


    // items
    public void addItem(Item item) {
        inventory.addItem(item);
    }

    public void removeItem(Item item) {
        inventory.removeItem(item);
    }

    public void getALLItem() {
        inventory.getAllItems();
    }

    public Item findItem(String name) {
        return inventory.findItem(name);
    }


    // characters
    public void addCharacter(Character cha) {
        characters.addCharacter(cha);
    }

    public void removeCharacter(Character cha) {
        characters.removeCharacter(cha);
    }

    public void getAllCharacters() {
        characters.getAllCharacters();
    }

    public Character findCharacter(String name) {
        return characters.findCharacter(name);
    }

    // position
    public String getDescription() {return description;}



}
