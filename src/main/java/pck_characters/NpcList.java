package pck_characters;

import pck_items.Item;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Siripatsornsirichai on 1/29/2018 AD.
 */
public class NpcList {

    private List<Character> characters = new ArrayList<Character>();

    public void addCharacter(Character cha){
        characters.add(cha);
    }

    public void removeCharacter(Character cha) {
        characters.remove(cha);
    }

    public void getAllCharacters() {
        for (Character cha: characters) {
            System.out.print(cha.getName()+", ");
        }
        System.out.println();
    }

    public Character findCharacter(String item) {
        for (Character cha: characters) {
            if (cha.getName().equals(item)) {
                return cha;
            }
        }
        return null;
    }

}
