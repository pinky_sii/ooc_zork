package pck_characters;

/**
 * Created by Siripatsornsirichai on 1/28/2018 AD.
 *
 * A non-playable character
 *
 */
public class Character {
    private String name;
    private int HP;
    private int attackPower;


    public Character(String name, int HP) {
        this.name = name;
        this.HP = HP;
        attackPower = 10;
    }

    public String getName(){
        return this.name;
    }

    public int getHP() {
        return this.HP;
    }

    public void setHP(int m) {
        this.HP-= m;
    }

    public int getAttackPower(){
        return  this.attackPower;
    }

}
